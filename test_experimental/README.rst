Files
====

exp_testing.py
  Uses the network to predict atoms in experimental images.
AnalyseSingleExperimentalImage.ipynb
  Python notebook to predict atoms in single experimental images.
analyze_expt_movie_with_network.py
  Analyze an image sequence with a neural network.
postprocess_expt_movie.py
  Post-process the output from the script above to generate a movie.
