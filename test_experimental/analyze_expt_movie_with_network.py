"""Analyze a movie of electron microscopy files with neural network.

This is step one of generating a movie from a sequence of HRTEM
images.  The images must be a collection of .dm4, .dm3 or .png files
in a folder or a tree of folders (the default Hour/Minute/Second
folder structure often used for .dm4 files will work fine).

This script will run all images through a neural network, and save the
output in parallel folder structures.  If the images are in a folder
IMAGES, then the raw output will be places in an identical folder
structure named IMAGES_prediction.  Peak analysis will be run on this
data, and this is placed in IMAGES_atoms.

Further processing can then be done e.g. with the script
postprocess_expt_movie.py.
"""

import sys
sys.path.insert(0, '../')

import matplotlib.pyplot as plt
from glob import glob
import numpy as np
import tensorflow as tf
from temnn.loaddata import load_CNN
from temnn.knet import Unet
#from temnn.knet import MSDnet
from temnn.data.dataset import DataEntry,DataSet
from temnn.data.mods import local_normalize
from pyqstem.imaging import CTF
import matplotlib.pyplot as plt
# Peak detection
from stm.preprocess import normalize
from stm.feature.peaks import find_local_peaks, refine_peaks
from skimage.morphology import disk
from scipy.spatial import cKDTree as KDTree
import sys
import os
import shutil
#from collections import deque
import hyperspy.api as hs
import skimage.io
import argparse
from natsort import natsorted
import json

cnn = None
cnn_size = (-1, -1)

def analyze_tree(input, net, cnnfile, sampling, threshold, filetype='.dm4'):
    if input.endswith('/'):
        input = input[:-1]

    output1 = input + '_prediction'
    output2 = input + '_atoms'
    for o in (output1, output2):
        if not os.path.exists(o):
            print("Creating folder", o)
            os.makedirs(o)
        # Keep a copy of this script for reference
        shutil.copy2(__file__, o)

    for root, dirs, files in os.walk(input, followlinks=True):
        dirs.sort()
        files.sort()
        for f in files:
            if f.endswith(filetype):
                infile = os.path.join(root, f)
                print("Analyzing", infile, flush=True)
                prediction, atoms = analyze(infile, net, cnnfile, sampling, threshold)
                assert root.startswith(input)
                bname = os.path.splitext(f)[0]
                outdir = output1 + root[len(input):]
                predname = os.path.join(outdir, bname+'_prediction')
                print("  -> {}.npz".format(predname), flush=True)
                if not os.path.exists(outdir):
                    os.makedirs(outdir)
                np.savez_compressed(predname, prediction=prediction)
                outdir = output2 + root[len(input):]
                atomname = os.path.join(outdir, bname+'_atoms')
                print("  -> {}.npy".format(atomname), flush=True)
                if not os.path.exists(outdir):
                    os.makedirs(outdir)
                np.save(atomname, atoms)

def analyze(filename, net, cnnfile, sampling, threshold):
    global cnn, cnn_size
    if filename.endswith('.dm4') or filename.endswith('*.dm3'):
        # Electron microscopy file - read with HyperSpy
        f = hs.load(filename)
        # Image size must be divisible by 8
        image_size = f.data.shape
        image_size = tuple(np.array(image_size) // 8 * 8)
        image = f.data[:image_size[0],:image_size[1]]
    else:
        #Assume an image file, read with scikit-image
        f = skimage.io.imread(filename)
        image = skimage.color.rgb2gray(f)
        image_size = image.shape
        image_size = tuple(np.array(image_size) // 8 * 8)
        image = image[:image_size[0],:image_size[1]]

    # Load CNN if not yet loaded or not fitting.
    if cnn_size != image_size:
        print("Loading CNN of size {} from {}".format(image_size, cnnfile))
        cnn = load_CNN(cnnfile, net, image_size)[1]
        cnn_size = image_size
        
    # Normalize the image so it can be recognized
    img2 = local_normalize(image, 120, 120)
    img2.shape = (1, ) + image_size + (1,)

    # Make the prediction
    prediction = cnn.predict(img2)[0,:,:,0]

    # Find the peaks in the output from the CNN
    distance = int(2.5 / sampling)
    peaks = find_local_peaks(prediction, min_distance=distance, 
                             threshold=threshold, exclude_border=10,
                             exclude_adjacent=True)
    peaks = refine_peaks(normalize(prediction), peaks, 
                         disk(2), model='polynomial')

    return prediction, peaks

parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("network",
                    help="The folder where the trained neural network is placed")
parser.add_argument("datafolder",
                    help="The path and name of the folder where the experimental data is placed.")
parser.add_argument("--filetype",
                    help="The file extension of the experimental data (.dm4, .dm3, .png).",
                    default='.dm4')
parser.add_argument("--sampling",
                    help="Sampling in Angstrom/pixed (default: use what the CNN was trained with)",
                    default=0, type=float)
parser.add_argument("--threshold",
                    help="Threshold for detecting atoms in CNN output",
                    default=0.4, type=float)
parser.add_argument("--networkpattern",
                    help="The pattern for finding the neural network parameters (latest matching is used, default 'clusters-*.h5')",
                    default="clusters-*.h5")
args = parser.parse_args()

graph_glob = os.path.join(args.network, args.networkpattern)
graph_files = glob(graph_glob)
print("Found {} files matching {}".format(len(graph_files), graph_glob))
if not graph_files:
    raise RuntimeError("No trained network found in "+graph_glob)
cnnfile = list(natsorted(graph_files))[-1]

# Identify neural network based on foldername
NNname = args.network.partition("_")[0][3:]
# Import the specified neural network
if NNname == 'Unet':
    net = Unet
elif NNname == 'MSDnet':
    net = MSDnet
else:
    raise RuntimeError('Please specify a correct Neural Network instead of '+NNname)
print('Using {} architecture'.format(NNname))

# Get the sampling from command line or from the training JSON file
sampling = args.sampling
if not sampling:
    parameterfile = os.path.join(args.network, 'parameters.json')
    with open(parameterfile, "rt") as pfile:
        parameters = json.load(pfile)
    sampling = np.mean(parameters['sampling'])


print("Experimental data in {} (file type {})".format(args.datafolder, args.filetype))

analyze_tree(args.datafolder, net.graph, cnnfile, sampling, args.threshold, args.filetype)
