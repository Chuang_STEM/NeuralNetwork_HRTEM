import sys
sys.path.insert(0, '..')  # for importing modules from parent directory

import maker
from flake import Flake
from temnn.reproducible import make_reproducible
from ase import Atoms
from ase.build import mx2
import ase.db
import numpy as np
import argparse
import os
import time
import sys
import shutil
from multiprocessing import Pool

class DatabaseMakerNoDistinction(maker.SampleMaker):
    """Makes samples of 2D materials from a database.  All atoms are the same class.

    All columns are in the same class.
    """
    use_cfg = False  # Write as .traj to keep prototype info.
    def __init__(self, size, seed, database, indices, repetitions):
        """Create the maker object.

        Parameters:
        size: Size of the system (two numbers in Å)
        seed: Seed for the random number generator
        database: File name of the database
        repetitions: Number of systems generated from each database entry
        """
        self.size = size
        self.rng = np.random.default_rng(seed)
        self.repetitions = repetitions
        self.db = ase.db.connect(database, use_lock_file=False, serial=True)
        if indices is None:
            self.ids = np.array([row.id for row in self.db.select()])
        else:
            self.ids = indices
        assert len(self.ids) <= len(self.db)

    @property
    def database_size(self):
        return len(self.ids)

    def make_data(self, id):
        atoms = self.make_atoms(id)

        # Get positions directly from the atoms.
        pos = atoms.get_positions()
        atno = atoms.get_atomic_numbers()
        positions = pos[:,:2]
        classes = np.zeros(len(atoms), int)
        return atoms, positions, classes

    def make_atoms(self, index):
        number = index // self.repetitions
        repetition = index % self.repetitions
        prototype = self.db.get_atoms(id=int(self.ids[number]))
        name = prototype.get_chemical_formula(mode='hill')
        self.flake = Flake(prototype, self.size, self.rng)
        self.flake.make()
        self.flake.vacancies(0.05)
        self.flake.holes(0.05)
        self.flake.perturb_positions(0.01)
        self.flake.rotate()
        self.flake.tilt(10) 
        atoms = self.flake.get_atoms()
        atoms.info['prototype'] = name
        return atoms

def makeFromDatabase(first_number, last_number, indices, dir_name, npoints, sampling, seed,
                        database, repetitions):
    size = npoints * sampling
    print("Generating images {} - {}".format(first_number, last_number))
    print("Output folder:", dir_name)
    print("Number of point in wave function: {} x {}".format(npoints, npoints))
    print("Sampling: {} Å/pixel".format(sampling))
    print("System size: {:.1f} Å x {:.1f} Å".format(size, size))
    print("Seed:", seed, flush=True)
    maker = DatabaseMakerNoDistinction((size, size), seed, database, indices, repetitions)
    maker.run(first_number, last_number, dir_name, npoints, sampling)

# The main function is copied and modified from maker.py
def main(driverfunction):
    """Main script driver.  

    Requires one arguments, the driver function.
    """
    parser = argparse.ArgumentParser()
    positionalparams = ('folder', )
    parser.add_argument("folder", help="The name of the folder (in ../simulation_data) where the output is placed.")
    parser.add_argument("--repetitions", type=int, default=1,
                        help="The desired number of training examples PER DATABASE ENTRY.")
    #parser.add_argument("-s", "--start", type=int, default=0,
    #                    help="Starting value (if some training data was already made).")
    parser.add_argument("--database", default='c2db.db',
                        help="Database file (default: c2db.db).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument("--sampling", default=0.05, type=float,
                        help="Sampling of wave function in Å/pixel")
    parser.add_argument("--arraysize", default=1024, help="Size of wavefunction array")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator")
    args = parser.parse_args()

    # Initialize the RNG
    seed = np.random.SeedSequence(args.seed)
    seed_for_splitting, seed_train, seed_test = seed.spawn(3)
    rng = np.random.default_rng(seed_for_splitting)
    
    # Check if the database is present, open it and calculate the number of images from it.
    # Also, split into training and test sets, such that a specific combination of elements only appears
    # in one of them.
    if not os.path.exists(args.database):
        print("ERROR: Database file '{}' does not exist.".format(args.database))
        if args.database == 'c2db.db':
            print("You can download it from http://c2db.fysik.dtu.dk")
        sys.exit(1)
    testset = []
    trainingset = []
    with ase.db.connect(args.database) as database:
        database_size = len(database)
        if not database_size:
            raise ValueError("Database '{}' appears to be empty".format(args.database))
        print("Database contains {} materials.  Splitting it.".format(database_size), flush=True)
        seen = dict()
        ids = [row.id for row in database.select()]
        for i in ids:
            atoms = database.get_atoms(id=i)
            symbols = tuple(sorted(set(atoms.get_chemical_symbols())))
            if symbols in seen:
                istestset = seen[symbols]
                #print("                                OLD:", symbols, istestset)
            else:
                istestset = rng.random() <= 1.0/3
                seen[symbols] = istestset
                #print("NEW:", symbols, istestset)
            if istestset:
                testset.append(i)
            else:
                trainingset.append(i)
    testset = np.array(testset)
    trainingset = np.array(trainingset)
    print("Split into training set ({} materials) and test set ({} materials)".format(
        len(trainingset), len(testset)), flush=True)
    number_train = len(trainingset) * args.repetitions
    number_test = len(testset) * args.repetitions
    
    #if args.numproc > number - args.start:
    #    raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('../', 'simulation_data', args.folder)

    print("Generating samples {} to {} in folder {} using {} process(es)".format(
        0, number_train, dir_name, numproc))
    print("... and samples {} to {} in folder {} using {} process(es)".format(
        0, number_test, dir_name+'-test', numproc), flush=True)

    # Make sure working folders exist
    for d in (dir_name, dir_name + '-test'):
        if not os.path.exists(d):
            print("Creating folder", d, flush=True)
            for subf in ['wave', 'model', 'points']:
                os.makedirs(os.path.join(d, subf))

    # Keep a copy of this script for reference
    shutil.copy2(__file__, dir_name)
    shutil.copy2(maker.__file__, dir_name)
    shutil.copy2(args.database, dir_name)
    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['seed'] = seed.entropy
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce.sh'), parser.prog, argdict, positionalparams)

    before = time.time()

    for dname, ndata, indices, s in ((dir_name, number_train, trainingset, seed_train),
                                     (dir_name+'-test', number_test, testset, seed_test)):
        
        if numproc == 1:
            # Running on a single core.
            driverfunction(0, ndata, indices, dname, args.arraysize, args.sampling, s,
                               args.database, args.repetitions)
        else:
            data = []

            # To work around a memory leak, do not make more than 20 data
            # sets in a single process before forking a new one.
            maxperprocess = 20
            minjobs = ndata // maxperprocess
            numtasks = numproc * (1 + minjobs // numproc)
            seeds = s.spawn(numtasks)
            for i in range(numtasks):
                data.append((
                    i * ndata // numtasks,
                    (i+1) * ndata // numtasks,
                    indices,
                    dname,
                    args.arraysize,
                    args.sampling,
                    seeds[i],
                    args.database,
                    args.repetitions
                    ))
            with Pool(numproc, maxtasksperchild=1) as pool:
                pool.starmap(driverfunction, data)

    print("Time to simulate models: {:.2f} s.".format(time.time() - before))


    
if __name__ == "__main__":
    main(makeFromDatabase)
    
