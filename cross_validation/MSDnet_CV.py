import sys
sys.path.insert(0, '../')

import os
import matplotlib
matplotlib.use('AGG')
import json
from temnn.knet import MSDnet
import numpy as np
import matplotlib.pyplot as plt
from sklearn import preprocessing
import tensorflow.keras as keras
from tensorflow.keras.models import Model
from tensorflow.keras import layers, regularizers
from tensorflow.keras.optimizers import RMSprop, Adam
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor, KerasClassifier
from sklearn.model_selection import RandomizedSearchCV, KFold, cross_val_score

def create_model(optimizer=RMSprop()):
    with open('../simulation_data/MoS2_supported/parameters.json') as json_file:
        parameters = json.load(json_file)

    image_size = tuple(parameters['image_size']) # spatial dimensions of input/output
    if 'multifocus' in parameters:
        image_features = parameters['multifocus'][0]
    else:
        image_features = 1 # depth of input data
    num_classes = parameters['num_classes'] # number of predicted class labels

    x = keras.Input(shape=image_size+(image_features,))
    model = MSDnet.graph(x, output_features=num_classes)

    model.compile(optimizer, 
                loss='categorical_crossentropy',
                metrics=['accuracy'])
    return model

model = KerasClassifier(build_fn = create_model,
                       epochs=1,
                       batch_size=200)
# define the random search parameters
opt = ['rmsprop', 'adam']
params = dict(optimizer=opt)
random_search = RandomizedSearchCV(model,
                                   param_distributions = params,
                                   cv = 3)

## Load data
datafolder = '../simulation_data/MoS2_supported/'
with open(os.path.join(datafolder, 'parameters.json')) as json_file:
    parameters = json.load(json_file)
imagedir = os.path.join(datafolder, 'images_labels')
# Read number of training images
images_per_epoch = parameters['images_per_epoch']
image_epochs = parameters['image_epochs']

## Determine number of GPUS
cudavar = 'CUDA_VISIBLE_DEVICES'
if cudavar in os.environ:
    cudadevices = os.environ[cudavar]
    numgpus = len(cudadevices.split(','))
    print(cudavar, '=', cudadevices)
    print("Found {} GPU devices".format(numgpus))
else:
    numgpus = 1  
# The model is pretty big, we probably can only train on one data
# point at a time without running out of GPU memory.
batch_size = numgpus * 2
image_size = tuple(parameters['image_size']) # spatial dimensions of input/output
if parameters.get('multifocus', None):
    image_features = parameters['multifocus'][0]
else:
    image_features = 1 # depth of input data
num_classes = parameters['num_classes'] # number of predicted class labels
num_in_epoch = images_per_epoch//batch_size

images = np.empty((images_per_epoch, image_size[0], image_size[1], image_features),
                      np.float32)
labels = np.empty((images_per_epoch, image_size[0], image_size[1], num_classes),
                      np.float32)
rng = np.random.default_rng()
num_epochs = epoch = 0
summary = None
batch_no = 1
print("Loading training epoch {} / {}.".format(epoch, num_epochs), flush=True)
for i in range(images_per_epoch):
    data = np.load(os.path.join(imagedir,
                    'image_label_{:03d}_{:04d}.npz'.format(0, i)))
    images[i] = data['image']
    labels[i] = data['label']
    if (i + 1) % 500 == 0:
        print("    loaded {} images.".format(i+1), flush=True)
# Random permutation
permut = rng.permutation(images_per_epoch)
images = images[permut]
labels = labels[permut]

print('Images shape {}'.format(images.shape))
print('Labels shape {}'.format(labels.shape))
random_search_results = random_search.fit(images, labels)

print("Best Score: ", random_search_results.best_score_, "and Best Params: ", random_search_results.best_params_)
means = random_search_results.cv_results_['mean_test_score']
stds = random_search_results.cv_results_['std_test_score']
params = random_search_results.cv_results_['params']
for mean, stdev, param in zip(means, stds, params):
    print(f' mean={mean:.4}, std={stdev:.4} using {param}')
