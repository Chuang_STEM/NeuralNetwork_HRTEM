import sys
sys.path.insert(0, '../')

import numpy as np
import json
from temnn.imagesimul.makeimages import MakeImages
from temnn.loaddata import load
from PIL import Image
import os
import shutil
import argparse

# We generate different images for each epoch.  It is not necessary to
# use all image_epochs when training, and it is possible to reuse
# them.  In test data, this is overwritten to 1.
image_epochs = 10

parameters = {
    # Size of the images during training (x, y)
    'image_size': (216, 216),

    # Number of classes in output of network, including the background
    # class.  Setting num_classes=1 means just a single class, no
    # background.  Otherwise, num_classes should be one higher than
    # the number of actual classes, to make room for the background
    # class.
    'num_classes': 4,

    # Image resolution range in pixels/Angstrom
    'sampling': (0.08, 0.10),

    # Defocus range in Angstrom
    'defocus': (0, 200),

    # Focal series if not None.  The three numbers are number of images, change in focus, random part of change.
    'multifocus': (3, 30.0, 1.0),
    
    # Cs range in Angstrom (1 micrometer = 1e4 A)
    'Cs': (-15e4, 15e4),

    # Range of the logarithm (base 10) of the dose in electrons/A^2
    'log_dose': (2.5, 5),
    
    # Range of A22 aberration magnitude
    'a22': (0, 50),

    # Range of focal spread
    'focal_spread': (20, 40),

    # Range of blur
    'blur': (0, 2.),

    # Range of MTF parameters (same names as in paper, except c0 which is 1.0 in paper).
    'mtf_c0': (1.0, 1.0),
    'mtf_c1': (0, 0.1),
    'mtf_c2': (0.3, 0.6),
    'mtf_c3': (2.0, 3.0),

    # normalization distance in Angstrom
    'normalizedistance': 12.0,

    # Spot size in Angstrom
    'spotsize': 0.4,

    # How many images to save in debug folder (None=none, True=all).
    'debug': 50,
    
    # If a seed to the random number generator should be used
    # Can be set to True to generate a random seed that is reused in testing run
    # or set to a specific seed that is used for reproducible training.
    # Setting it to None disable deterministic testing as well.
    'seed': [998616271, 738814483, 960996334, 235280404, 560394456],
}

if __name__  == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data_dir", help="Path to directory with exit waves.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.set_defaults(train=True, test=False)
    args = parser.parse_args()
    
    ## Directories 
    data_dir =  args.data_dir
    if args.test==True:
        data_dir += '-test'
        # Only one epoch in the test data
        image_epochs = 1
    if args.train==True:
        data_dir += '/'

    parameters['image_epochs'] = image_epochs
    
    out_dir = data_dir
    print('Output directory:', out_dir)
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    image_dir = os.path.join(out_dir, 'images_labels')
    if not os.path.exists(image_dir):
        os.makedirs(image_dir)
    #label_dir = out_dir + 'labels/'
    #if not os.path.exists(label_dir):
    #    os.makedirs(label_dir)
    debug_dir = out_dir + 'debug/'
    if not os.path.exists(debug_dir):
        os.makedirs(debug_dir)
    # Keep a copy of this script for reference
    shutil.copy2(__file__, out_dir)
    
    parameters['data_dir'] = data_dir
    parameters['debug_dir'] = debug_dir  # Make it accessible to MakeImages

    image_size = parameters['image_size']
    sampling = np.mean(parameters['sampling'])
    data = load(data_dir)
    n = len(os.listdir(data_dir + 'wave'))
    print("Number of images:", n)
    parameters['images_per_epoch'] = n
    # Peak shape defines the shape of the label peaks. This can be
    # 'Gaussian' or 'Disk', which will optimise the loss function
    imagestream = MakeImages(data, parameters, peak_shape='Disk', seed=parameters['seed'])
    
    # Also store the parameters in a machine_readable file
    with open(os.path.join(out_dir, "parameters.json"), "wt") as pfile:
        json.dump(parameters, pfile, sort_keys=True, indent=4)
        
    i = 0
    for epoch in range(image_epochs):
        for b in range(n):
            print("Generating image", i, "of", n*image_epochs, flush=True)
            image, label = imagestream.next_example()
            filename = os.path.join(image_dir, 'image_label_{:03d}_{:04d}'.format(epoch, b))
            print('Image and label saved as ', filename, flush=True)
            np.savez_compressed(filename, image=np.asarray(image), label=np.asarray(label))
            i += 1
