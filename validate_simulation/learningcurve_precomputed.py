"""Calculate precision and recall on the testset."""
import sys
sys.path.insert(0, '../')

import matplotlib
matplotlib.use('AGG')

from operator import itemgetter
import random
from glob import glob
import numpy as np
#from numpy import load
import tensorflow.keras as keras
from tensorflow.keras.utils import multi_gpu_model
import tensorflow as tf
#from temnn.knet import Unet
#from temnn.imagesimul.makeimages import MakeImages
from temnn.loaddata import load, load_CNN
from tensorflow.keras.models import load_model
# Peak detection
from temnn.imagesimul.evaluatepeaks import precision_recall, evaluate_result
##import sys
import os
from multiprocessing import Pool
import shutil
from natsort import natsorted
import json

# This script takes one argument: the name of the folder where the
# trained neural network is placed.
if len(sys.argv) >= 2:
    graph_dir = sys.argv[1]
else:
    print("\n\nUsage: {} foldername".format(sys.argv[0]), file=sys.stderr)
    sys.exit(-1)

# Identify neural network based on foldername
NNname = graph_dir.partition("_")[0][3:]
print('Importing', NNname)
# Import the specified neural network
if NNname == 'Unet':
    from temnn.knet import Unet
    net = Unet
    print('Validating Unet architecture')
elif NNname == 'MSDnet':
    from temnn.knet import MSDnet
    net = MSDnet
    print('Validating MSDnet architecture')
else:
    print('Please specify a correct Neural Network (Unet, or MSDnet).')

graph_path = os.path.join(graph_dir, 'model-{}')
result = os.path.join(graph_dir, 'learningcurve.dat')
parameterfile = os.path.join(graph_dir, 'parameters.json')

with open(parameterfile, "rt") as pfile:
    parameters = json.load(pfile)

# Even if training generated debug images, we do not want to do it now.
parameters['debug'] = False

# The validation data is in a sister-folder to the training data
ddir = parameters['data_dir']
if ddir.endswith('/'):
    ddir = ddir[:-1]
    vd = ddir + '-test/'
parameters['validation_dir'] = vd

# We only need one GPU, and since we do not train we can have more images in a batch.
num_gpus = 1
batch_size = 8 * num_gpus

# Load data
image_size = tuple(parameters['image_size'])

ddir_train = parameters['data_dir']
print('Importing training data from:', ddir_train)
imagedir_train = os.path.join(ddir_train, 'images_labels')
numimages_per_epoch_train = parameters['images_per_epoch']
numimageepochs_train = parameters['image_epochs']
print("Number of training images: {} x {} = {}".format(
    numimageepochs_train, numimages_per_epoch_train,
    numimageepochs_train * numimages_per_epoch_train))

ddir_valid = parameters['validation_dir']
print('Importing validation data from:', ddir_valid)
imagedir_valid = os.path.join(ddir_valid, 'images_labels')
with open(os.path.join(ddir_valid, 'parameters.json')) as p2file:
    param2 = json.load(p2file)
numimages_per_epoch_valid = param2['images_per_epoch']
numimageepochs_valid = param2['image_epochs']
print("Number of validation images: {} x {} = {}".format(
    numimageepochs_valid, numimages_per_epoch_valid,
    numimageepochs_valid * numimages_per_epoch_valid))

# Keep a copy of this script for reference
shutil.copy2(__file__, graph_dir)

# The ImageStream objects have discovered how many cpus we can use
#maxcpu = imagestream_train.maxcpu
if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
    maxcpu = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
    print("Setting max number of CPUs to", maxcpu, flush=True)
else:
    maxcpu = None

# We read the number of images and the number of classes from the parameter file.
num_classes = parameters.get('num_classes', 1)
try:
    num_images = parameters['multifocus'][0]
except (KeyError, TypeError):
    num_images = 1

# Find all the CNN parameter files
#print("Looking for CNNs in files matching", graph_path)
#cnnfiles = list(natsorted(glob(graph_path)))
numtrainepochs = parameters['train_epochs']
#print("Found {} CNN parameter files".format(len(cnnfiles)))
#cnnfiles = list(enumerate(cnnfiles))
#while len(cnnfiles) > 30:
    # Keep every second file, but make sure to lose the first rather than the last.
#    cnnfiles = cnnfiles[::-2][::-1]


sampling = np.mean(parameters['sampling'])

# Preread all images, as we will use the same images for each epoch.
# If the number of images is later increased to be proportional to the
# number of epochs, then we should only take a corresponding sample,
# but still keep it constant.
def loadimages(imagedir, numperepoch, numepoch):
    images = []
    labels = []
    for ep in range(numepoch):
        for b in range(numperepoch):
            if b % numepoch == 0: # Take one image for every epoch
                data = np.load(os.path.join(imagedir, 'image_label_{:03d}_{:04d}.npz'.format(ep, b)))
                images.append(data['image'])
                labels.append(data['label'])
    return np.concatenate(images), np.concatenate(labels)
    
images_train, labels_train = loadimages(imagedir_train, numimages_per_epoch_train, numimageepochs_train)
images_valid, labels_valid = loadimages(imagedir_valid, numimages_per_epoch_valid, numimageepochs_valid)

with open(result, "wt") as outfile:
    if num_classes == 1:
        line = "{:3s}  {:8s}  {:8s}  {:8s}  {:8s}".format(
            "n", "T-prec.", "T-recall", "V-prec.", "V-recall")
    else:
        line = "{:3s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}  {:8s}".format(
            "n", "T-prec.", "T-recall", "T-xP", "T-xR", "V-prec.", "V-recall", "V-xP", "V-xR")
    print("*****", line, flush=True)
    print(line, file=outfile, flush=True)
    i = 0
    for step in np.arange(0,numtrainepochs):
        print("Evaluating CNN step {}/{} in {}".format(i+1, numtrainepochs, graph_path.format(step+1)), flush=True)
        #x, model = load_CNN(gr, net.graph, image_size, num_gpus=num_gpus,
        #                    image_features=num_images, num_classes=num_classes)
        model = load_model(graph_path.format(step)) 
        linedata = [step+1]
        for (images, labels) in ((images_train, labels_train), (images_valid, labels_valid)):
            print("Making predictions with CNN.", flush=True)
            predictions = model.predict(np.array(images), batch_size=batch_size)

            # Now we have an array with predicted images (predictions) and
            # one with expected images (labels).  We now need to calculate
            # precision and recall in parallel
            print("Predictions shape {}, Labels shape {}".format(predictions.shape, labels.shape))
            print("Processing predictions.", flush=True)
            result = []
            with Pool(maxcpu) as pool:
                result = pool.starmap(evaluate_result, 
                                      zip(predictions, labels, [sampling]*len(labels)))
            
            result = np.array(result)
            precision = result[:,0].mean()
            recall = result[:,1].mean()
            if num_classes == 1:
                linedata.extend((precision, recall))
            else:
                cross_p = result[:,2].mean()
                cross_t = result[:,3].mean()
                linedata.extend((precision, recall, cross_p, cross_t))

        i += 1
        if num_classes == 1:
            line = "{:3d}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}".format(*tuple(linedata))
        else:
            line = "{:3d}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}".format(*tuple(linedata))
        print("*****", line, flush=True)
        print(line, file=outfile, flush=True)
        #imagestream_train.reset()
        #imagestream_valid.reset()
