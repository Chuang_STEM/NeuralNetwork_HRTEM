Files
====

learningcurve_onthefly.py
  Calculates the precision and recall for the simulated
  training data set and simulated validation (test)
  dataset. Images are generated on the fly.
learningcurve_precomputed.py
  Calculates the precision and recall for the simulated
  training data set and simulated validation (test)
  dataset. precomputed images are loaded in.
validation_plot.py/validation_plot.ipynb
  Plots the precision and recall.
learningcurve.bsub
  Sends Python script to the HPC GPU.
learningcurve.dtucompute.sh
  Sends Python script to dtu compute GPU.

