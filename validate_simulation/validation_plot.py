import numpy as np
import matplotlib.pyplot as plt

#path = '../Unet_trained_data/Au_cluster-110-2deg5-big/'
path = '../Unet_precomputed_multi_trained_data/MoS2_supported_test/'
#path = './'
f = path + 'learningcurve.dat' 

data = np.genfromtxt(f,
                     dtype=None)

legend = data[0,1:].astype('str')
epoch = data[1:,0].astype('int')
vals = np.genfromtxt(f,
                     skip_header=1,
                     usecols=(1,2,3,4))

plt.figure(figsize=(4, 3))
plt.rc('font', family='serif')
plt.rc('xtick', labelsize='x-small')
plt.rc('ytick', labelsize='x-small')
for i in np.arange(vals.shape[1]):
    plt.plot(epoch, vals[:,i])
plt.legend(legend)
plt.xlim([1,np.size(epoch)])
plt.xlabel('Epoch')
plt.tight_layout()
plt.savefig(path + 'learningcurve.eps')
plt.show()
