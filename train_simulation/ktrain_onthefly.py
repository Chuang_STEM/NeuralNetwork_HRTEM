import sys
sys.path.insert(0, '../')

import matplotlib
matplotlib.use('AGG')
import os
from datetime import datetime
from glob import glob
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.utils import multi_gpu_model
#from temnn.knet import net
from temnn.data.dataset import DataSet, DataEntry
from temnn.imagesimul.makeimages import MakeImages
from temnn import loaddata
#import sys
import os
import time
import platform
import shutil
import json
import argparse
import contextlib

num_epochs = 12 # number of training epochs
save_epochs = 1
#loss_type = 'binary_crossentropy' # mse or binary_cross_entropy
loss_type = 'mse' # mse or binary_cross_entropy
optimizer = 'rmsprop'
metrics=['accuracy']

parameters = {
    # Size of the images during training (x, y)
    'image_size': (216, 216), # MoS2
    #'image_size': (512, 512), # Au nanoparticles

    # Number of classes in output of network, including the background
    # class.  Setting num_classes=1 means just a single class, no
    # background.  Otherwise, num_classes should be one higher than
    # the number of actual classes, to make room for the background
    # class.
    'num_classes': 4, # MoS2
    #'num_classes': 1, #Au nanoparticles

    # Image resolution range in pixels/Angstrom
    'sampling': (0.09, 0.12),

    # Defocus range in Angstrom
    'defocus': (0, 200), # MoS2
    #'defocus': (20, 200), # Au nanoparticles
    
    # Focal series if not None.  The three numbers are number of images, change in focus, random part of change.
    'multifocus': (3, 30.0, 1.0), # MoS2
    
    # Cs range in Angstrom (1 micrometer = 1e4 A)
    'Cs': (-15e4, 15e4),

    # Range of the logarithm (base 10) of the dose in electrons/A^2
    'log_dose': (2.5, 5), # MoS2
    #'log_dose': (2, 4.5), # Au nanoparticles
    
    # Range of A22 aberration magnitude
    'a22': (0, 50),

    # Range of focal spread
    'focal_spread': (20, 40),

    # Range of blur
    'blur': (0, 2.), # MoS2
    #'blur': (0, 3.0), # Au nanoparticles

    # Range of MTF parameters (same names as in paper, except c0 which is 1.0 in paper).
    'mtf_c0': (1.0, 1.0),
    'mtf_c1': (0, 0.1),
    'mtf_c2': (0.3, 0.6),
    'mtf_c3': (2.0, 3.0),

    # normalization distance in Angstrom
    'normalizedistance': 12.0,

    # Spot size in Angstrom
    'spotsize': 0.4,
    
    # How many images to save in debug folder (None=none, True=all).
    'debug': 50,

    # If a seed to the random number generator should be used
    # Can be set to True to generate a random seed that is reused in testing run
    # or set to a specific seed that is used for reproducible training.
    # Setting it to None disable deterministic testing as well.
    'seed': [998616271, 738814483, 960996334, 235280404, 560394456],
}
    

def summary_image(y,size):
    return tf.reshape(tf.cast(tf.argmax(y,axis=3),tf.float32),(1,)+size+(1,))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("NNname", help="The name of the neural network being used.")
    parser.add_argument("datafolder", help="The path and name of the folder where the data is placed (remember to finish with /).")
    parser.add_argument("folder", help="The name of the folder (in ../(NNname)_trained_data/)where the output is placed.")
    args = parser.parse_args()
    
    # Print on which host this is running (useful for troubleshooting on clusters).
    print("{}: Running on host '{}'".format(
        datetime.now().strftime("%Y%m%d-%H%M%S"),
        platform.node()
    ))

    # A number of output files and folders contain a timestamp as part of their name.
    foldername = args.NNname + '_onthefly_trained_data'
    folderlabel = os.path.join('../', foldername, args.folder)
   
    if not os.path.exists(folderlabel):
        print("Creating folder", folderlabel)
        os.makedirs(folderlabel)

    timestamp = datetime.now().strftime("%Y%m%d-%H%M%S")
    summary_dir = "summaries/" + timestamp + "/"
    graph_path = folderlabel + '/model-{}'
    debug_dir = "debug/" +  timestamp
    data_dir = loaddata.find_training_folders(args.datafolder)
    parameters['data_dir'] = data_dir
    parameters['debug_dir'] = os.path.join(folderlabel,debug_dir)  # Make it accessible to MakeImages

    # A seed for the random number generator may be given, for
    # deterministic runs.  In that case the validation is made
    # deterministic too.
    #
    # A seed value of True means generate a random seed, but store it
    # so the run can be repeated in a deterministic way.
    if parameters['seed'] is True:
        parameters['seed'] = list(np.random.randint(2**30, size=5))
    if parameters['seed']:
        parameters['seed_validate'] = parameters['seed'] + [1, 42]
    else:
        parameters['seed_validate'] = None
        assert parameters['seed'] is None   # Any other false value means trouble.

    # Make folders for output
    graph_dir = os.path.dirname(graph_path)
    if graph_dir and not os.path.exists(graph_dir):
        os.makedirs(graph_dir)
    if parameters['debug']:
        os.makedirs(os.path.join(folderlabel,debug_dir))
    logfile = open(os.path.join(graph_dir, timestamp + '.log'), "wt", buffering=1)
        
    # Keep a copy of this script for reference
    shutil.copy2(__file__, graph_dir)

    # Also store the parameters in a machine_readable file
    with open(os.path.join(graph_dir, "parameters.json"), "wt") as pfile:
        json.dump(parameters, pfile, sort_keys=True, indent=4)

    # Load training data
    data = loaddata.load_training(data_dir)

    # Determine number of GPUS
    cudavar = 'CUDA_VISIBLE_DEVICES'
    if cudavar in os.environ:
        cudadevices = os.environ[cudavar]
        numgpus = len(cudadevices.split(','))
        print(cudavar, '=', cudadevices)
        print("Found {} GPU devices".format(numgpus))
    else:
        numgpus = 1
        
    # The model is pretty big, we probably can only train on one data
    # point at a time without running out of GPU memory.
    batch_size = numgpus * 2

    image_size = parameters['image_size'] # spatial dimensions of input/output
    if 'multifocus' in parameters:
        image_features = parameters['multifocus'][0]
    else:
        image_features = 1 # depth of input data
    num_classes = parameters['num_classes'] # number of predicted class labels
    #if num_classes > 1:
    #    print('loss type: categorical_crossentropy')
    #    loss_type = 'categorical_crossentropy'
    #else:
    #    print('loss type: binary_crossentropy')
    #    loss_type = 'binary_crossentropy'
   
    num_in_epoch = data.num_examples//batch_size
    num_iterations=num_epochs*num_in_epoch
    
    outputcounter = 0

    assert(batch_size % numgpus == 0)

    # We now generate a stream of training images
    imagestream = MakeImages(data, parameters, peak_shape='Gaussian', seed=parameters['seed'])

    # Create the neural network
    # Import the specified neural network
    print('Importing', args.NNname)
    if args.NNname == 'Unet':
        from temnn.knet import Unet
        net = Unet
        print('Training Unet architecture')
    elif args.NNname == 'MSDnet':
        from temnn.knet import MSDnet
        net = MSDnet
        print('Training MSDnet architecture')
    else:
        print('Please specify a correct Neural Network (Unet, or MSDnet)')

    if numgpus > 1:
        strategy = tf.distribute.MirroredStrategy()
        strategy_scope = strategy.scope
        print("*** Replicas:", strategy.num_replicas_in_sync)
        print(strategy)
        assert strategy.num_replicas_in_sync == numgpus
    else:
        strategy_scope = contextlib.nullcontext
    with strategy_scope():
        x = keras.Input(shape=image_size+(image_features,))
        model = net.graph(x, output_features=num_classes)
        model.compile(optimizer=optimizer, loss=loss_type,
                      metrics=metrics)

    print("Model summary:", args.NNname)
    print(model.summary())
    if not os.path.exists(os.path.join(folderlabel,summary_dir)):
        os.makedirs(os.path.join(folderlabel,summary_dir))
    
    print("Starting timing")
    before = time.time()

    # Optimize the model, saving every save_epoch epoch.
    for epoch in np.arange(0,num_epochs):
        summary = None
        for i in range(num_in_epoch):
            image,label = imagestream.next_example()
            if batch_size > 1:
                image = [image]
                label = [label]
                for b in range(1, batch_size):
                    img2, lbl2 = imagestream.next_example()
                    image.append(img2)
                    label.append(lbl2)
                image = np.concatenate(image)
                label = np.concatenate(label)

            # Train
            with strategy_scope():
                h = model.train_on_batch(image, label)
            if summary is None:
                summary = np.array(h)
            else:
                summary += h

            # Print where we are
            print("Epoch: {}/{} Batch: {}/{}   [{}/{}]".format(epoch+1, num_epochs,
                                                               i+1, num_in_epoch,
                                                               (i+1 + epoch*num_in_epoch)*batch_size,
                                                               num_iterations*batch_size),
                      flush=True)
        # Save 
        if (epoch+1) % save_epochs == 0:
            #model.save_weights(graph_path.format(epoch))
            model.save(graph_path.format(epoch))
        print(epoch+1, *tuple(summary))
        print(epoch+1, *tuple(summary), file=logfile)
    
    totaltime = time.time() - before
    print("Time: {} sec  ({} hours)".format(totaltime, totaltime/3600))
           
