"""Make nanoparticles by cutting using spherical harmonics."""


## for importing modules from parent directories
import sys
sys.path.insert(0, '../../')
sys.path.insert(0, '../../NeuralNetwork_HRTEM')

## import modules
import numpy as np
from scipy.special import sph_harm
from pyqstem.util import atoms_plot
from temnn.imagesimul.labels import project_positions,create_label
from temnn.reproducible import make_reproducible
import matplotlib.pyplot as plt
from scipy.cluster.hierarchy import fcluster, linkage
from pyqstem import PyQSTEM
from pyqstem.imaging import CTF
from ase.io import write
import ase.build
import os
import time
import shutil
import argparse
from multiprocessing import Pool
import subprocess

def driver2(args):
    driver(**args)
    
def driver(first_number, last_number, dir_name, N, sampling, element, rmax, tilt, lmax, seed):
    """Create nanoparticles, their exit waves and the ground truth for training.

    This function is creating a sequence of nanoparticles.  It is
    intended to be called in multiple processes in parallel, so the
    start and end number of the sequence is specified.

    first_number: int
        Sequence number of the first nanoparticle to be generated.
    last_number: int
        One more than the last nanoparticle to be generated.
    dir_name: string
        Name of the folder where the output is placed.
    N: int
        Size of the wavefunction array (will be N*N 128-bit complex numbers).
    sampling: float
        Resolution of the wavefunctions, in Angstrom per pixel.
    element: str
        Chemical element of the nanoparticle
    rmax: float
        Maximal typical radius of the nanoparticle.  Should be less 
        than 0.33 * sampling * N.
    tilt: float
        Maximal off-axis tilt of nanoparticle, in degrees.  1.0 to 2.0 are good values.
    lmax: int
        Maximum l of the spherical harmonic expansion of the random shapes.
    seed: np.random.SeedSequence or similar suitable seed
        Seed for the random number generator.
    """
    # Parameters
    L=sampling*N
    num_classes=1

    if rmax > 0.33 * sampling * N:
        print("WARNING: Nanoparticles are likely to be too big, and to be truncated.")

    rng = np.random.default_rng(seed)
    qstem=PyQSTEM('TEM')

    # Create the models
    for i in range(first_number, last_number):
        radius = rmax * (0.5 + 0.5 * rng.random())

        coefficients = make_coefficients(lmax, amplitude=1.0, power=2, rng=rng)
        atoms = nanoparticle(element, radius, coefficients)

        # Rotate to a 110 zone axis
        atoms.rotate(v='x',a=90.0, center='COU')
        # Random in-plane rotation
        atoms.rotate(v='z', a=rng.random()*360, center='COU')

        # Cut the nanoparticle, if it is too large.
        limit = 3.0  # Minimal amount of space along edges.
        atoms.center(vacuum=0)
        size=np.diag(atoms.get_cell())
        atoms.center()
        if size.max() > L - 2*limit:
            print("WARNING: System too big, should have size {} but has {}.".format(
                L - 2 * limit, str(size)))
            n = len(atoms)
            pos = atoms.positions
            keep = np.ones(len(atoms), bool)
            for j in range(3):
                a = pos[:,j]
                keep *= (a > limit) * (a < L - limit)
            atoms = atoms[keep]
            atoms.center(vacuum=0)
            size=np.diag(atoms.get_cell())
            print("         Removed {} of {} atoms.".format(n - len(atoms), n))
        atoms.set_cell((L,)*3)

        # Find the atomic columns
        positions=atoms.get_positions()
        clusters = fcluster(linkage(positions[:,:2]), 1, criterion='distance')
        unique,indices=np.unique(clusters, return_index=True)


        omega=rng.random()*360
        alpha=rng.random()*tilt

        atoms.rotate(v='z', a=omega, center='COU')
        atoms.rotate(v='y', a=alpha, center='COU')
        atoms.rotate(v='z', a=-omega, center='COU')

        # Move random amount in [limit, L - size[.] - limit]
        tx = limit + (L - size[0] - 2 * limit) * rng.random()
        ty = limit + (L - size[1] - 2 * limit) * rng.random()

        # Set the size of the cell.  In principle the height of the
        # cell (in the z direction) should vary according to the size
        # of the nanoparticle, but that will cause a memory corruption
        # in PyQSTEM.  Instead, we align the nanoparticles near the
        # *top* of the cell, as the electron beam enters from the
        # bottom.
        atoms.set_cell((L, L, L))
        atoms.translate((tx, ty, L - size[2] - limit))

        # Find the positions of the columns and the number of atoms per column.
        positions=atoms.get_positions()[:,:2]
        sites = np.array([np.mean(positions[clusters==u],axis=0) for u in unique])
        heights = np.array([np.sum(clusters==u) for u in unique])

        #positions,counts=project_positions(atoms,distance=.8,return_counts=True)

        wave_size=(int(atoms.get_cell()[0,0]*10),int(atoms.get_cell()[1,1]*10))
        qstem.set_atoms(atoms)
        qstem.build_wave('plane',300,wave_size)
        qstem.build_potential(int(atoms.get_cell()[2,2]*2))
        qstem.run()
        wave=qstem.get_wave()

        wave.array=wave.array.astype(np.complex64)

        positions,counts=project_positions(atoms,distance=.8,return_counts=True)
        label=create_label(positions/sampling,(N,)*2,width=12,num_classes=num_classes,null_class=False)

        np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i), sites=sites, heights=heights)
        wave.save('{0}/wave/wave_{1:04d}.npz'.format(dir_name,i))
        write('{0}/model/model_{1:04d}.cfg'.format(dir_name,i),atoms)

        print('iteration', i, flush=True)

# Helper functions for generating the nanoparticle

def make_coefficients(lmax, amplitude, power, rng):
    """Generate an expansion in spherical harmonics.

    The roughness of the resulting nanoparticle are given by the
    parameters.  The values ``amplitude=1.0, power=2`` give a rather
    smooth nanoparticle, the values ``amplitude=0.5, power=1`` a much
    rougher particle.

    The coefficients of each Y_lm will be a Gaussian with spready
    ``amplitude / l**power``.
    """
    lmax = 7
    c = []
    for l in range(lmax+1):
        if l <= 1:
            ampl = 0.0
        else:
            ampl = 0.5/l
        c.append(rng.normal(0.0, ampl, size=(2*l+1,)))
    return c
        
def nanoparticle(element, radius, coefficients):
    """Create a nanoparticle from spherical harmonics.
    
    Creates a nanoparticle by cutting a shape defined by the
    coefficients of the various (real) spherical harmonics Ylm.

    The nanoparticle is oriented as defined by ``ase.build.bulk`` when
    the `òrthorhombic=True`` flag is passed.  For a cubic crystal, there
    will be a <110> direction along the x and y axes, and a <100>
    direction along the z axis.
    
    element: str or Atoms
        The kind of nanoparticle to create.  Either a chemical symbol
        or an orthorhombic unit cell as a prototype.
    radius: float
        The average radius of the nanoparticle, in Angstrom.
        coefficients: sequence of sequences of float
        The weights of the Y_lm functions are coefficients[l][l+m].
    """
    
    if isinstance(element, str):
        element = ase.build.bulk(element, orthorhombic=True)
    # Make sure the unit cell is sensible, and find its size.
    c = element.cell[:]
    size = np.zeros(3)
    for i in range(3):
        for j in range(3):
            if i == j:
                size[i] = c[i,j]
                assert size[i] > 0.1
            else:
                assert np.isclose(c[i,j], 0.0)
    
    # Make a block, and cut the nanoparticle
    repeat = 1 + (4 * radius // size).astype(int)
    atoms = element.repeat(repeat)
    # Positions as polar coordinates relative to center of mass
    com = np.mean(atoms.positions, axis=0)
    pos = atoms.positions - com
    r = np.sqrt((pos * pos).sum(axis=1))
    theta = np.arctan2(np.sqrt(pos[:,0]**2 + pos[:,1]**2), pos[:,2])
    phi = np.arctan2(pos[:,1], pos[:,0])
    
    # Cut the nanoparticle
    keep = r < radius * f(coefficients, theta, phi)
    atoms = atoms[keep]
    x, y, z = atoms.positions.T
    #print("Making nanoparticle:  cell =", atoms.cell)
    #print("    x € [{:.1f}, {:.1f}]    y € [{:.1f}, {:.1f}]    z € [{:.1f}, {:.1f}]".format(
    #    x.min(), x.max(), y.min(), y.max(), z.min(), z.max()))
    return atoms

        
def Ylm(l, m, theta, phi):
    """Real-valued Spherical Harmonic (Y_lm).

    Defined from the complex-valued spherical harmonics in scipy.  Note
    that compared to the usual notation in Physics (which this function
    follows), the scipy versions switch the order of l and m, and switch
    the order of theta and phi.
    """
    
    if m == 0:
        return sph_harm(m, l, phi, theta).real    # Imaginary part is zero.
    elif m < 0:
        return np.sqrt(2) * (-1)**m * sph_harm(m, l, phi, theta).imag
    else:
        return np.sqrt(2) * (-1)**m * sph_harm(m, l, phi, theta).real

def f(c, theta, phi):
    "Radial function as specified as an expansion on spherical harmonics."
    r = 1.0
    for l, lval in enumerate(c):
        assert len(lval) == 2*l+1
        for mm, v in enumerate(lval):
            m = mm - l
            #print(l, m, v)
            r = r + v * Ylm(l, m, theta, phi)
        lval = np.array(lval)
        #print("Weight (l={}): {:.3f}".format(l, np.sqrt((lval*lval).sum())))
    return r
    


def_resolution=1200
def_sampling = 0.05
lmax = 7


if __name__ == "__main__":
    # Use the standard argument parser - but also save the names of the positional parameters for
    # creating the reproducibility file.
    parser = argparse.ArgumentParser()
    positionalparams = ('folder', 'element', 'rmax', 'tilt', 'number')
    parser.add_argument("folder", help="The name of the folder (in ../data) where the output is placed.")
    parser.add_argument("element", help="Chemical symbol of the desired material.", type=str)
    parser.add_argument("rmax", help="Maximal radius of the nanoparticles.", type=float)
    parser.add_argument("tilt", help="Maximal off-axis tilt of the nanoparticle", type=float)
    parser.add_argument("number", type=int,
                        help="The desired number of training/testing examples (incl. any that are already done).")
    parser.add_argument("--sampling", type=float, default=def_sampling,
                        help="Sampling of wave function in Angstrom per pixel")
    parser.add_argument("--resolution", type=int, default=def_resolution,
                            help="Resolution of wave function in pixels.")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value (if some training data was already made).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    if args.numproc > args.number - args.start:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('../', 'simulation_data', args.folder)
    seed = np.random.SeedSequence(args.seed)
    
    print("Generating samples {} to {} in folder {} using {} process(es)".format(
        args.start, args.number, dir_name, numproc))
    print("np.random.SeedSequence entropy:", seed.entropy)
    print("Element: '{}'   R_max = {:.3f} A/px    l_max = {}".format(args.element, args.rmax, lmax))
        
    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))

    # Keep a copy of this script for reference ...
    shutil.copy2(__file__, dir_name)
    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['seed'] = seed.entropy
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce.sh'), parser.prog, argdict, positionalparams)
    before = time.time()

    if numproc == 1:
        # Running on a single core.
        driver(first_number=args.start, last_number=args.number, dir_name=dir_name, N=args.resolution,
               sampling=args.sampling, element=args.element, rmax=args.rmax, lmax=lmax, tilt=args.tilt, seed=seed)
    else:
        data = []
        ndata = args.number - args.start
        seeds = seed.spawn(numproc)
        for i in range(numproc):
            data.append(dict(
                first_number=args.start + i * ndata // numproc,
                last_number = args.start + (i+1) * ndata // numproc,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                element=args.element,
                rmax=args.rmax,
                lmax=lmax,
                tilt=args.tilt,
                seed=seeds[i]
                ))
        with Pool(numproc) as pool:
            pool.map(driver2, data)

    print("Time to simulate models: {:.2f} s.".format(time.time() - before))
