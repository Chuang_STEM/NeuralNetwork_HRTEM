from __future__ import absolute_import, print_function

from tensorflow.keras.models import Model
from tensorflow.keras import layers, regularizers
#from tensorflow import name_scope
#from tensorflow.keras.layers.advanced_activations import PReLU
from tensorflow.keras import initializers
from .ReflectionPadding2D import ReflectionPadding2D

weight_decay = None

def graph(x, output_features, channels=1, kernel_size=9, layers=100,
            reflection_padding=False, dilation_rate=1, limit=8):
    """Define the Convolutional Neural Network, an MSD-Net.

    Parameters:

    output_features: The number of output feature channels (including an
    eventual background channel).

    channels: The number of feature channels in the MSD-Net.

    layers = Number of layers in the MSD-net

    dilation_rate: The initial dilation of the kernel
    
    limit: Max dilation rate
    """
    # Dilations will iteratively increase till limit 
    # and then reset to 1
    lim = limit
    dil = 1
    # Input for layers
    y = x
    for i in range(layers):
        # Construct a layer of the MSD-net
        convol = conv_layer(y, channels, dilation_rate*dil,
                            kernel_size, reflection_padding,
                            name="conv{}".format(i+1)) 
        concat = conc(y, convol, name="conc{}".format(i+1))
        # Input to next layer
        y = concat
        # Iterate or reset dilation
        dil += 1
        if dil > lim:
           dil = 1
    
    inference = score_layer(y, channels=output_features)
    return Model(x, inference)
  
def conv_layer(x, channels, dilation_rate, kernel_size,
                reflection_padding, name='conv'):
    """A single convolutional layer."""
    if reflection_padding == True:
        # Reflection Padding
        x = ReflectionPadding2D(padding=(dilation_rate*(kernel_size-1),
                                        dilation_rate*(kernel_size-1)))(x)
        padding = 'valid'
    else:
        padding = 'same'
    conv = layers.Conv2D(channels, kernel_size, padding=padding,
                             dilation_rate=dilation_rate,
                             kernel_regularizer=regul(),
                             kernel_initializer='RandomNormal',
                             bias_initializer=get_bias_init(),
                             name=name)
    x = prelu(conv(x), name=name)
    # Normalize
    return layers.BatchNormalization(name=name+'_b_norm')(x)

def conc(x, y, name):
    return layers.concatenate([x, y], name=name)

def score_layer(x, channels, name="score"):
    "The final layer."
    if channels > 1:
        act = 'softmax'
    else:
        act = 'sigmoid'
    conv = layers.Conv2D(channels, kernel_size=1,
                             activation=act,
                             padding='same',
                             kernel_regularizer=regul(),
                             kernel_initializer='RandomNormal',
                             bias_initializer=get_bias_init(),
                             name=name)
    return conv(x)

# Regularization
def regul():
    if weight_decay:
        return regularizers.l2(weight_decay)
    else:
        return None

def get_bias_init():
    return initializers.Constant(0.1)

# Parametric RELU activiation
def prelu(x, name=None):
    p = layers.PReLU(shared_axes=[1,2], name=name+"/PReLU", 
                     alpha_initializer=initializers.Constant(0.01))
    return p(x)
